<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Simple_login
{
    protected $CI;


    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->model('login_model');
    }
    //fungsi login
    public function login($email,$password)
    {
        //CAMBIAR m_login a M_login
        $check=$this->CI->login_model->login($email,$password);
//jika ada data login maka session login dibuat

if ($check) {
    $idUsuario         = $check->idUsuario;
    $nombre       =$check->nombre;
    $email   =$check->correoElectronico;
    $rol   =$check->rol;
    //buat session
    $this->CI->session->set_userdata('idUsuario',$idUsuario);
    $this->CI->session->set_userdata('nombre',$nombre);
    $this->CI->session->set_userdata('correoElectronico',$email);
      $this->CI->session->set_userdata('correoElectronico',$email);
    //redirect ke halaman admin

    redirect(base_url('inicio/index'),'refresh');

}
else
{
     $this->CI->session->set_flashdata('warning', 'usuario y contraseña invalidos');

     redirect(base_url('login'),'refresh');
}
}
public function cek_login()
{
if($this->CI->session->userdata('correoElectronico')==""){
    $this->CI->session->set_flashdata('warning', 'No has iniciado sesión');
    redirect(base_url('login'),'refresh');

}
}
public function logout()
{
//meambuag semua session yg telah dibuat tadi saat login berhasil

$this->CI->session->unset_userdata('idUsuario');
$this->CI->session->unset_userdata('nombre');
$this->CI->session->unset_userdata('correoElectronico');
    //setelah session di unset maka redirect ke halaman login

    $this->CI->session->set_flashdata('sukses', 'Has cerrado la sesión correctamente');
    redirect(base_url('login'),'refresh');

}

}

?>
