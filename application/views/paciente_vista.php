<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> Registro</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?= base_url()?>assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= base_url()?>assets/plugins/iCheck/square/blue.css">

</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href=""><b>HELP</b>LIFE</a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Regístrate en HelpLife</p>

    <form action="<?php echo site_url('paciente/create')?>" method="POST">
      <div class="form-group has-feedback">
        <input type="text" name="nombre" class="form-control" placeholder="Nombre "   required>
        <span class="glyphicon glyphicon-star form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" name="apellido" class="form-control" placeholder="Apellido"   required>
        <span class="glyphicon glyphicon-star form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" name="sexo" class="form-control" placeholder="sexo"   required>
        <span class="glyphicon glyphicon-star form-control-feedback"></span>
      </div>
      <!--  <select class="form-control">
          <option value="" name="sexo"disabled selected>Sexo</option>
            <option value="Femenino" name="femenino">Femenino</option>
            <option value="Masculino"name="masculino">Masculino</option>
          </select>-->
    <br>
      <div class="form-group has-feedback">
        <label >Fecha de Nacimiento</label>
          <input type="date" name="fechaNac" class="form-control" placeholder="Fecha Nacimiento"  required>
          <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
        </div>
      <div class="form-group has-feedback">
        <!--<input type="text" name="nombre" class="form-control" placeholder="Alergias"   required>-->
        <textarea name="alergias"placeholder="Alergias"class="form-control" rows="5" cols="40"></textarea>
      </div>
      <div class="form-group has-feedback">
        <input type="email" name="email" class="form-control" placeholder="Email"  required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Contraseña" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>

      <div class="row">
        <div class="container">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" name="check" required> Acepto los <a href="#">términos y condiciones</a>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="container">
          <button type="submit" name="registrar" class="btn btn-primary btn-block btn-flat" value="save">Registrarme</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <br>
    <a href="login.php" class="text-center">Tengo actualmente una cuenta</a>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 2.2.3 -->
<script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="assets/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
