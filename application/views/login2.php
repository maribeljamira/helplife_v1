<script>
function getFocus() {
  document.getElementById("salida").focus();
  startArtyom();
  artyom.say("Por favor diga su nombre de usuario")
}

function getFocus2() {
  document.getElementById("salida2").focus();
  artyom.say("Por favor diga su contraseña")
}


function loseFocus() {
  document.getElementById("salida ").blur();
}
artyom.addCommands([
			{
        indexes:['ver inicio'],
				action: function(i){
					if (i==0) {
						artyom.say("Abriendo inicio");
						window.open("http://localhost/ProSuca/home/",'_self');

          }

				}
      },
      {
				indexes:['limpiar'],
				action: function(){
					$('#salidsa').val('');
				}
			}
		]);
</script>


<br>
<div class="container">
  <div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6">

<!-- Material form login -->
<div class="card">
  <h5 class="card-header btn aqua-gradient white-text text-center py-4">
    <strong><h3 class="release">Iniciar Sesión</h3></strong>
  </h5>
  <div align="right">
  <a class="btn-floating aqua-gradient   material-tooltip-main data-toggle="tooltip" data-placement="left" title="Iniciar Microfono"  onclick="getFocus();"><i class="fas fa-microphone"></i></a>
  </div>
  <div class="avatar mx-auto white">
    <img src="<?=base_url()?>assets/bootstrapAdmin/images/admin.gif" class="rounded-circle" alt="woman avatar">
  </div>
  <!--Card content-->
  <div class="card-body px-lg-10 pt-0">

    <!-- Form -->
    <?php
        //NOTIFIACIONES ERROR
        echo validation_errors('<div class="alert alert-warning" role="alert">','</div>');
        //NOTIFICACION

        if ($this->session->flashdata('warning')) {
                echo '<div class="alert alert-warning">';
                echo $this->session->flashdata('warning');
                echo '</div>';
        }
        //notificacion del login
        if($this->session->flashdata('sukses')){
            echo '<div class="alert alert-warning">';
            echo $this->session->flashdata('sukses');
            echo '</div>';
        }
            // cambiamo la direccion de base url por login   admin/inputsekolah
        echo form_open(base_url('login'),'class="form-horizontal" entype="multipart/formdata"');
        ?>



      <div class="md-form">
        <input  type="text" name="username" id="salida" class="form-control release" onclick="getFocus();"  required />
        <label for="materialLoginFormEmail" class="release">Nombre de usuario</label>
      </div>



      <!-- Password -->
      <div class="md-form">
        <input type="password" name="password" id="salida2" class="form-control release" onclick="getFocus2();" required>
        <label for="materialLoginFormPassword" class="release">Ingrese su contraseña</label>
      </div>

      <div class="d-flex justify-content-around">
        <div>
          <!-- Remember me -->
          <div class="form-check">
            <input type="checkbox" class="form-check-input" id="materialLoginFormRemember">
            <label class="form-check-label release" for="materialLoginFormRemember" >Recuérdame</label>
          </div>
        </div>
      </div>
      <!-- Sign in button -->
      <button type="submit" class="btn blue-gradient btn-rounded btn-block my-4 waves-effect z-depth-0 release" type="submit" id="hola">Acceder</button>
    </form>
    <!-- Form -->
    <?php echo form_close(); ?>
  </div>
</div>
<!-- Material form login -->


    </div>
    <div class="col-sm-3"></div>
  </div>
  <div>

<br>
<br>
<br>
