

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>login</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="<?= base_url()?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="<?= base_url()?>assets/bootstrap/css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="<?= base_url()?>assets/bootstrap/style.css" rel="stylesheet">
</head>
<body style="background-image: url(assets/img/img2.jpg); background-size: cover;">
  <div class="container">
    <div class="row">
      <div class="col-sm">
      </div>
      <div class="col-sm">
        <!--registro modal-->
        <br>
        <br>
        <br>
        <div class="card">
        <h5 class="card-header info-color white-text text-center py-4">
          <strong>Iniciar Sesion</strong>
        </h5>
              <?php
              //NOTIFIACIONES ERROR
              echo validation_errors('<div class="alert alert-warning" role="alert">','</div>');
              //NOTIFICACION

              if ($this->session->flashdata('warning')) {
                      echo '<div class="alert alert-warning">';
                      echo $this->session->flashdata('warning');
                      echo '</div>';
              }
              //notificacion del login
              if($this->session->flashdata('sukses')){
                  echo '<div class="alert alert-warning">';
                  echo $this->session->flashdata('sukses');
                  echo '</div>';
              }
                  // cambiamo la direccion de base url por login   admin/inputsekolah
              echo form_open(base_url('login'),'class="form-horizontal" entype="multipart/formdata"');
              ?>
        <!--Card content-->
        <div class="card-body px-lg-5 pt-0">
          <!-- Form -->
          <form method="POST" accept-charset="utf-8" class="text-center" style="color: #757575;" action="<?php base_url()?>login/index">
            <!-- Email -->
            <div class="md-form">
              <input type="email" id="materialLoginFormEmail"  name="email" class="form-control">
              <label for="materialLoginFormEmail">E-mail</label>
            </div>
            <!-- Password -->
            <div class="md-form">
              <input type="password" id="materialLoginFormPassword" name="password"  class="form-control">
              <label for="materialLoginFormPassword">Password</label>
            </div>
            <div class="d-flex justify-content-around">
              <div>
                <!-- Remember me -->
                <div class="form-check">
                  <input type="checkbox" class="form-check-input" id="materialLoginFormRemember">
                  <label class="form-check-label" for="materialLoginFormRemember">Recordar Contrasena</label>
                </div>
              </div>
            </div>
            <!-- Sign in button -->
            <button class="btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0"id="hola" type="submit">Ingresar</button>
          </form>
          <?php echo form_close(); ?>
        </div>
      </div>
        <!--buttom modal-->
        <br>
        <br>
        <div class="modal fade" id="modalSubscriptionForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header text-center">
              <h4 class="modal-title w-100 font-weight-bold">Registrese</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body mx-3">
              <div class="text-center">
                <a href="<?= base_url('paciente/vista')?>" class="btn btn-default btn-rounded mb-4" >
                  Registrarse como Paciente</a>
              </div>
              <div class="text-center">
                <a href="<?= base_url('medico')?>" class="btn btn-default btn-rounded mb-4" >
                  Registrarse como Medico</a>
              </div>
            </div>
            <div class="modal-footer d-flex justify-content-center">
          </div>
        </div>
      </div>
    </div>
          <div class="text-center">
            <a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalSubscriptionForm">
              Registrarse --></a>
          </div>
      </div>
      <div class="col-sm">
      </div>
    </div>
  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="<?= base_url()?>assets/bootstrap/js/jquery-3.4.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="<?= base_url()?>assets/bootstrap/js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="<?= base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="<?= base_url()?>assets/bootstrap/js/mdb.min.js"></script>
</body>

</html>
