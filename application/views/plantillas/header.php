<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>CONSULTAS MEDICAS ONLINE</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->


  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="<?= base_url()?>assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="<?= base_url()?>assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?= base_url()?>assets/lib/animate/animate.min.css" rel="stylesheet">
  <link href="<?= base_url()?>assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
<!--  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?= base_url()?>assets/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="<?= base_url()?>assets/css/inicio.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: BizPage
    Theme URL: https://bootstrapmade.com/bizpage-bootstrap-business-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>

  <!--==========================
    Header
  ============================-->
  <header id="header">
    <div class="container-fluid">

      <div id="logo" class="pull-left">
        <h1><a href="#intro" class="scrollto">HelpLife</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="#intro"><img src="img/logo.png" alt="" title="" /></a>-->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="#intro">Inicio</a></li>
          <li><a href="#about">Servicios</a></li>
            <li><a href="<?=base_url()?>Upload_file">Medicos</a></li>
          <li class="menu-has-children"><a href="">My Perfil</a>
            <ul>
              <li><a href="<?= base_url('web2/facebook')?>">facebook</a></li>
              <li><a href="<?= base_url('web2/twitter')?>">twitter</a></li>
              <li><a href="<?= base_url('web2')?>">youtube</a></li>
              <li><a href="<?= base_url('web2/googlemaps')?>">Google Maps</a></li>
              <li><a href="<?= base_url('web2/sound')?>">Sound Cloud</a></li>
              <!--<li><a href="#">Mis Citas</a></li>
              <li><a href="#">Mis Consultas</a></li>
              <li><a href="#">Configuracion</a></li>-->
            </ul>

          <li><a href="#portfolio">Notificaciones</a></li>

          <li class="menu-has-children"><a href="">Ayuda</a>
            <ul>
              <li><a href="#">Servicio de ayuda</a></li>
              <li><a href="#">Reportar un Problema</a></li>
              <li><a href="#">configuracion</a></li>

            </ul>
          </li>
          <li><a href="#contact">Salir</a></li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->



  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  <script src="<?= base_url()?>assets/lib/jquery/jquery.min.js"></script>
  <!--<script src="assets/lib/jquery/jquery-migrate.min.js"></script>
  <script src="assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/lib/easing/easing.min.js"></script>-->
  <!--<script src="<?= base_url()?>assets/lib/superfish/hoverIntent.js"></script>-->
  <script src="<?= base_url()?>assets/lib/superfish/superfish.min.js"></script>
  <script src="<?= base_url()?>assets/lib/wow/wow.min.js"></script>
  <!--<script src="<?= base_url()?>assets/lib/waypoints/waypoints.min.js"></script>
  <script src="<?= base_url()?>assets/lib/counterup/counterup.min.js"></script>
  <!--<script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="<?= base_url()?>assets/lib/isotope/isotope.pkgd.min.js"></script>
  <!--<script src="lib/lightbox/js/lightbox.min.js"></script>-->
  <!--<script src="<?= base_url()?>assets/lib/touchSwipe/jquery.touchSwipe.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="<?= base_url()?>assets/contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="<?= base_url()?>assets/js/main.js"></script>

</body>
</html
