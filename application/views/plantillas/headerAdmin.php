<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="<?= base_url()?>assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="<?= base_url()?>assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/lib/animate/animate.min.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="<?= base_url()?>assets/css/inicio.css" rel="stylesheet">
    <script src="<?php echo site_url()?>assets/js/main.js"></script>

    </head>


    <!--==========================
      Header
    ============================-->
    <header id="header">
      <div class="container-fluid">

        <div id="logo" class="pull-left">
          <h1><a href="#intro" class="scrollto">HelpLife</a></h1>
          <!-- Uncomment below if you prefer to use an image logo -->
          <!-- <a href="#intro"><img src="img/logo.png" alt="" title="" /></a>-->
        </div>

        <nav id="nav-menu-container">
          <ul class="nav-menu">
            <li class="menu-active"><a href="<?= base_url('usuario')?>">usuario</a></li>
            <li><a href="<?= base_url('especialidad')?>">especialidad</a></li>
              <li><a href="<?= base_url('paciente')?>">Paciente</a></li>
              <li><a href="<?= base_url('medico')?>">Medico</a></li>
            <li ><a href="<?= base_url('medico')?>">citas</a>
              <li ><a href="">My Perfil</a>


            <li><a href="#portfolio">Notificaciones</a></li>

            <li class="menu-has-children"><a href="">Ayuda</a>
              <ul>
                <li><a href="#">Servicio de ayuda</a></li>
                <li><a href="#">Reportar un Problema</a></li>
                <li><a href="#">configuracion</a></li>

              </ul>
            </li>
            <li><a href="#contact">Salir</a></li>
          </ul>
        </nav><!-- #nav-menu-container -->
      </div>
    </header><!-- #header -->



    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    <!-- Uncomment below i you want to use a preloader -->
    <!-- <div id="preloader"></div> -->

    <!-- JavaScript Libraries -->
    <script src="<?= base_url()?>assets/lib/jquery/jquery.min.js"></script>
    <script src="<?= base_url()?>assets/lib/jquery/jquery-migrate.min.js"></script>
    <script src="<?= base_url()?>assets/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url()?>assets/lib/easing/easing.min.js"></script>
    <script src="<?= base_url()?>assets/lib/superfish/hoverIntent.js"></script>
    <script src="<?= base_url()?>assets/lib/superfish/superfish.min.js"></script>
    <script src="<?= base_url()?>assets/lib/wow/wow.min.js"></script>
    <script src="<?= base_url()?>assets/lib/wow/wow.js"></script>
    <script src="<?= base_url()?>assets/lib/waypoints/waypoints.min.js"></script>
    <script src="<?= base_url()?>assets/lib/counterup/counterup.min.js"></script>
    <script src="<?= base_url()?>assets/lib/owlcarousel/owl.carousel.min.js"></script>
    <script src="<?= base_url()?>assets/lib/isotope/isotope.pkgd.min.js"></script>
    <script src="<?= base_url()?>assets/lib/lightbox/js/lightbox.min.js"></script>
    <script src="<?= base_url()?>assets/lib/touchSwipe/jquery.touchSwipe.min.js"></script>
    <!-- Contact Form JavaScript File -->
    <script src="<?php echo site_url()?>assets/contactform/contactform.js"></script>

    <!-- Template Main Javascript File -->
