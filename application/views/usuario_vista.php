<?php $this->load->view('plantillas/headerAdmin');?>
    <div class="container">
    <br>
    <br>
    <br>
    <br>
    <br>

        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Agregar
        </button>
    <br>
    <br>
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agregar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?php echo site_url('usuario/create')?>">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nombre</label>
                        <input type="text" class="form-control" name="nombre" aria-describedby="emailHelp" placeholder="Enter last name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Apellido</label>
                        <input type="text" class="form-control" name="apellido" aria-describedby="emailHelp" placeholder="Enter first name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">sexo</label>
                        <input type="text" class="form-control" name="sexo" aria-describedby="emailHelp" placeholder="Enter first name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">fechaNac</label>
                        <input type="date" class="form-control" name="fechaNac" aria-describedby="emailHelp" placeholder="Enter birthdate">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">email</label>
                        <input type="email" class="form-control" name="email" aria-describedby="emailHelp" placeholder="Enter contact no">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">password</label>
                        <input type="password" class="form-control" name="password" aria-describedby="emailHelp" placeholder="Enter bio">
                    </div>
                    <button type="submit" class="btn btn-primary" value="save">Submit</button>
                </form>
            </div>
            </div>
        </div>
        </div>


        <table class="table">
            <thead class="thead-dark">
                <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Apellido</th>
                <th scope="col">sexo</th>
                <th scope="col">fecha de Nacimiento</th>
                <th scope="col">email</th>
                <th scope="col">password</th>
                <th scope="col">perfil</th>
                <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($result as $row) {?>
                <tr>
                <th scope="row"><?php echo $row->idUsuario; ?></th>
                <td><?php echo $row->nombre; ?></td>
                <td><?php echo $row->apellido; ?></td>
                <td><?php echo $row->sexo; ?></td>
                <td><?php echo $row->fechaNacimiento; ?></td>
                <td><?php echo $row->correoElectronico; ?></td>
                <td><?php echo $row->password; ?></td>

                <td><img src="<?=base_url()?>upload/<?=$row->perfil;?>" alt="foto" width="50" height="50"></td>

                <td> <a href="<?php echo site_url('usuario/edit');?>/<?php echo $row->idUsuario;?>">Edit</a>

                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>

  </body>
</html>
