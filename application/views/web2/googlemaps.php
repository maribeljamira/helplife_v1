<?php $this->load->view('plantillas/header');?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Gmaps</title>
    <meta charset="utf-8">
    <meta name="description" content="Gmaps">
    <meta name="keywords" content="Gmaps">
    <meta name="author" content="Google.com">

<link rel="stylesheet" href="<?= base_url()?>assets/bootstrap/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="<?= base_url()?>assets/bootstrap/js/jquery-3.3.1.js"></script>

<!-- Popper JS -->
<script src="<?= base_url()?>assets/bootstrap/js/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="<?= base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>

<style type="text/css">
  #map{
    height: 400px;
    width: 100%;
  }
</style>

</head>
<body>
  <br>
  <br>
  <br>
  <br>
  <br>
<div class="container">
  <h1
  style="
  font-family: 'Roboto Mono', monospace;
  text-align:center;"
  >GOOGLE MAPS</h1>
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Cochabamba</h4>

            <div id="map"></div>


        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  divmapa=document.getElementById('map');
  function initMap(){
    //opciones del mapa
    var options={ //menor zoom + lejos
      zoom:14,
      center:{lat:-17.379131,lng:-66.161832}
    }

    //nuevo mapa
    var map =new google.maps.Map(divmapa,options);

    addMarker({
    coordenadas:{lat:-17.379131,lng:-66.161832},
    iconImage:'http://maps.google.com/mapfiles/ms/micons/sportvenue.png',
    content:'<h1>Felix Capriles</h1>'
    }); //estadio
    addMarker({
      coordenadas:{lat:-17.385858,lng:-66.163511},
      iconImage:'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'
    }); //parque de la familia
    addMarker({
    coordenadas:{lat:-17.391592,lng:-66.182051},
    iconImage:'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
    content:'<h1>Hipodromo</h1>'
    }); //hipodromo
    addMarker({
    coordenadas:{lat:-17.383150,lng:-66.159785}
    }); //burger king
    addMarker({
    coordenadas:{lat:-17.385283,lng:-66.158166}
    }); //Comteco

    //Funcion Add marker
    function addMarker(props){
      var marker=new google.maps.Marker({
      position: props.coordenadas,
      map: map,
      //icon: props.iconImage
    });

      //Control de icono personalizado
      if(props.iconImage){
        marker.setIcon(props.iconImage);
      }

      //Control de InfoWindow
      if(props.content){
      var infoWindow=new google.maps.InfoWindow({
      content: props.content
      });

      marker.addListener('click',function(){
      infoWindow.open(map, marker);
      });

      }

    }

  }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCdri9ZPstuudUcaNo2QKuTLHnuVqPuBFM&callback=initMap"
    async defer></script>

  </body>
</html>
