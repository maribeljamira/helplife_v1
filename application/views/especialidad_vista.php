<?php $this->load->view('plantillas/headerAdmin');?>
    <div class="container">
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>

        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Agregar
        </button>
    <br>
    <br>
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agregar Nueva Especialidad</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?php echo site_url('especialidad/create')?>">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nombre</label>
                        <input type="text" class="form-control" name="nombre" aria-describedby="emailHelp" placeholder="Enter last name">
                    </div>




                    <button type="submit" class="btn btn-primary" value="save">Submit</button>
                </form>
            </div>
            </div>
        </div>
        </div>


        <table class="table">
            <thead class="thead-dark">
                <tr>
                <th scope="col">#</th>
                <th scope="col">nombre</th>

                <th scope="col">Fecha Registro</th>

                <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($result as $row) {?>
                <tr>
                <th scope="row"><?php echo $row->idEspecialidad; ?></th>
                <td><?php echo $row->nombre; ?></td>

                <td><?php echo $row->fechaReg; ?></td>

                <td> <a href="<?php echo site_url('especialidad/edit');?>/<?php echo $row->idEspecialidad;?>">Edit</a>  |
                   <a href="<?php echo site_url('especialidad/delete');?>/<?php echo $row->idEspecialidad;?>">Delete</a> </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>

  </body>
</html>
