<?php $this->load->view('plantillas/headerUsuario');?>
<!--Navbar-->
<br>
<br>
<br>
<br>
<br>

<div class="container">
  <div class="row">
    <div class="col-sm">

      <div class="card testimonial-card">
        <h1 class="card-title">Bienvenido</h1>
        <h4 class="card-title">My perfil</h4>
        <!-- Background color -->
        <div class="card-up indigo lighten-1"></div>

        <!-- Avatar -->
        <div class="avatar mx-auto white">
          <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20%2810%29.jpg" class="rounded-circle" alt="woman avatar">
        </div>
        <!-- Content -->
        <div class="card-body">
          <!-- Name -->
          <h4 class="card-title"><?php echo $this->session->userdata('nombre')?>-<?php echo $this->session->userdata('apellido')?></h4>
          <hr>
          <!-- Quotation -->

          <p>Email:<?php echo $this->session->userdata('email')?></p>
          <a href="<?=base_url()?>Upload_file" class="text-center">medico</a>


        </div>
      </div>
      <div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Sign in</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
        <div class="md-form mb-5">
          <i class="fas fa-envelope prefix grey-text"></i>
          <input type="email" id="defaultForm-email" class="form-control validate">
          <label data-error="wrong" data-success="right" for="defaultForm-email">Your email</label>
        </div>

        <div class="md-form mb-4">
          <i class="fas fa-lock prefix grey-text"></i>
          <input type="password" id="defaultForm-pass" class="form-control validate">
          <label data-error="wrong" data-success="right" for="defaultForm-pass">Your password</label>
        </div>

      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button class="btn btn-default">Login</button>
      </div>
    </div>
  </div>
</div>

<div class="text-center">
  <a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalLoginForm">Launch
    Modal Login Form</a>
</div>
    </div>
    <div class="col-sm">
      <h4 class="card-title">Medicos</h4>

        <div class="card mb-4">
          <?php foreach($usuario->result() as $row) {?>
          <!--Card image-->
          <div class="view overlay">
            <img class="card-img-top" src="<?=base_url()?>upload/<?=$row->perfil?>" alt="Card image cap">
            <a href="#!">
              <div class="mask rgba-white-slight"></div>
            </a>
          </div>

          <!--Card content-->
          <div class="card-body">

            <!--Title-->
            <h4 class="card-title">Doctor(a):<?php echo $row->nombre; ?>-<?php echo $row->apellido; ?></h4>
            <!--Text-->
            <div class="text-center">
              <a class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalSubscriptionForm">
                consultar Ahora</a>
            </div>
            <div class="text-center">
              <a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalSubscriptionForm">
                Reservar Cita</a>
            </div>
            <div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Sign in</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
        <div class="md-form mb-5">
          <i class="fas fa-envelope prefix grey-text"></i>
          <input type="email" id="defaultForm-email" class="form-control validate">
          <label data-error="wrong" data-success="right" for="defaultForm-email">Your email</label>
        </div>

        <div class="md-form mb-4">
          <i class="fas fa-lock prefix grey-text"></i>
          <input type="password" id="defaultForm-pass" class="form-control validate">
          <label data-error="wrong" data-success="right" for="defaultForm-pass">Your password</label>
        </div>

      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button class="btn btn-default">Login</button>
      </div>
    </div>
  </div>
</div>

<div class="text-center">
  <a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalLoginForm">Launch
    Modal Login Form</a>
</div>
          </div>
        <?php } ?>

        </div>

        <!--button modal-->
        <div class="modal fade" id="modalSubscriptionForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header text-center">
                  <h4 class="modal-title w-100 font-weight-bold">Consulte</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body mx-3">
                  <div class="text-center">
                    <a href="<?= base_url('paciente/vista')?>" class="btn btn-default btn-rounded mb-4" >
                      Consulta por Chat</a>
                  </div>
                  <div class="text-center">
                    <a href="<?= base_url('medico')?>" class="btn btn-default btn-rounded mb-4" >
                      Consultar por Videollamada</a>
                  </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
              </div>
            </div>
          </div>
        </div>

    </div>
    <div class="col-sm">
      <!--One of three columns-->
    </div>
  </div>
</div>
<!-- Card -->







  <a href="<?php echo site_url('usuario/edit');?>/<?php echo $row->idUsuario;?>">Consultar Ahora</a>
