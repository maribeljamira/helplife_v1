<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class usuario extends CI_Controller {
    public function __construct() {
        parent:: __construct();
        $this->load->model('usuario_model');
    }

	public function index() {
        $data['result'] = $this->usuario_model->getAllData();
		$this->load->view('usuario_vista', $data);
    }

    public function create() {
        $this->usuario_model->createData();
        redirect("usuario");
    }

    public function edit($id) {
        $data['row'] = $this->usuario_model->getData($id);
        $this->load->view('usuarioEditar', $data);
    }

    public function update($id) {
        $this->usuario_model->updateData($id);
        redirect("usuario");
    }

    public function delete($id) {
        $this->usuario_model->deleteData($id);
        redirect("usuario");
    }

}
