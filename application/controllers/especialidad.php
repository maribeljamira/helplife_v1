<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class especialidad extends CI_Controller {
    public function __construct() {
        parent:: __construct();
        $this->load->model('especialidad_model');
    }

	public function index() {
        $data['result'] = $this->especialidad_model->getAllData();
		$this->load->view('especialidad_vista', $data);
    }

    public function create() {
        $this->especialidad_model->createData();
        redirect("especialidad");
    }

    public function edit($id) {
        $data['row'] = $this->especialidad_model->getData($id);
        $this->load->view('especialidadEditar', $data);
    }

    public function update($id) {
        $this->especialidad_model->updateData($id);
        redirect("especialidad");
    }

    public function delete($id) {
        $this->especialidad_model->deleteData($id);
        redirect("especialidad");
    }

}
