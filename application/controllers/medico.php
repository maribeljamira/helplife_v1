<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class medico extends CI_Controller {

  public function vista() {

    $this->load->view('medico_vista');
  }
  public function regis() {

    $this->load->view('agregarMedico');
  }
  public function index() {

    $data['result'] = $this->medico_model->getAllData();

    $this->load->view('CRUDmedico',$data);

    }
  public function create() {
      $resultado =$this->medico_model->registrar();
      //redirect("login");
      if($resultado==true){
         redirect("medico/CRUDmedico");
      }else{
         redirect("medico");
      }
  }

}
