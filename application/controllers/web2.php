<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class web2 extends CI_Controller {

	public function index()
	{

		$this->load->view('web2/youtube');
		//$this->load->view('plantillas/footerAdmin');
	}
  public function twitter()
	{

		$this->load->view('web2/twitter');
		//$this->load->view('plantillas/footerAdmin');
	}
  public function facebook()
	{

		$this->load->view('web2/facebook');
		//$this->load->view('plantillas/footerAdmin');
	}
  public function googlemaps()
	{

		$this->load->view('web2/googlemaps');
		//$this->load->view('plantillas/footerAdmin');
	}
  public function sound()
	{

		$this->load->view('web2/sound');
		//$this->load->view('plantillas/footerAdmin');
	}

}
