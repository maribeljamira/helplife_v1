<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class paciente extends CI_Controller {
  public function __construct()
  {
      parent::__construct();
      $this->load->model('paciente_model');
      //$this->load->view('paciente_vista',$data);


  }
  public function vista() {

		$this->load->view('paciente_vista');
  }
  public function regis() {

		$this->load->view('agregarPaciente');
  }
  public function index() {

    $data['result'] = $this->paciente_model->getAllData();

    $this->load->view('CRUDpaciente',$data);

    }
  public function create() {
      $resultado =$this->paciente_model->registrar();
      //redirect("login");
      if($resultado==true){
         redirect("login");
      }else{
         redirect("paciente");
      }
  }


  }
