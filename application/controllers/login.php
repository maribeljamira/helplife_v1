
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('login_model');
        $this->load->model('File');

    }
    public function index()
    {
      $valid=$this->form_validation;
        $valid->set_rules('email', 'Email','required',array('required'=> ''));
        $valid->set_rules('password', 'password','required',array('required'=> ''));
        if ($valid->run()) {
          $email=$this->input->post('email');
          $password=$this->input->post('password');
          $res=$this->login_model->login($email,$password);
        }
        if(!$res){
          $this->session->set_flashdata('error','el correoElectronico o contrasena son incorrectos');
          redirect(base_url());
        }
        else {
          $data['usuario'] = $this->File->getAllData();
          $data= array(
        'id' =>$res->idUsuario,
        'nombre' =>$res->nombre,
        'apellido' =>$res->apellido,
        'email' =>$res->correoElectronico,
        'rol' =>$res->rol,
        'foto' =>$res->perfil,
        'login' =>TRUE);
      /*  $this->session->set_userdata($data);
      $this->session->userdata("rol")==2 //validamos las sesiones correctas del usuaario
        redirect('inicio/admin');
      $this->session->userdata("rol")==3 //validamos las sesiones correctas del usuaario
        redirect('inicio/index');*/


        redirect('inicio');
        }

        //$this->load->view('layouts/header');
      /*  $valid=$this->form_validation;
        $valid->set_rules('email', 'Email','required',array('required'=> ''));
        $valid->set_rules('password', 'password','required',array('required'=> ''));
        if ($valid->run()) {
            $email=$this->input->post('email');
            $password=$this->input->post('password');
            //prosess ke simple_login
            $this->simple_login->login($email, $password);
        }
        $data = array('TITLE' => 'login');
        $this->load->view('login_vista', $data, FALSE);
        //$this->load->view('layouts/footer');
    }
    public function logout()
    {
        $this->simple_login->logout();
    }*/
    }
      }

/*public function login(){
      $this->form_validation->set_rules('correoElectronico', 'correoElectronico', 'trim|required|xss_clean');
      $this->form_validation->set_rules('password', 'password', 'trim|required|xss_clean|callback_check_database');
      $correoElectronico = $this->input->post('correoElectronico');
      $password = $this->input->post('password');
      $result = $this->login_model->log($correoElectronico , $password);
      if($result){
              $sess_array = array();
              foreach($result as $row){
          $sess_array = array(
                 'email' => $row->correoElectronico,
                 'password' => $row->password,
                       'rol' => $row->rol
                );
                      $this->session->set_userdata('usuario', $sess_array);
      }
      if($this->session->userdata("rol")==2){ //validamos las sesiones correctas del usuaario
          redirect('inicio');
        }
          }else{
              echo "error";
          }

  }
  /*public function __construct()
  {
      parent::__construct();

	if (!$this->session->userdata("idUsuario") &&
        !$this->session->userdata("correoElectronico") &&
        !$this->session->userdata("password") &&
        !$this->session->userdata("rol"))
    {
        //Mensaje de error
        $this->session->set_flashdata("error", "Acceso Restringido");
        redirect('login'); //ruta a la que queremis que nos redireccione
    } else if($this->session->userdata("rol")==2){ //validamos las sesiones correctas del usuaario
        redirect('inicioPaciente');
    }else if($this->session->userdata("rol")==3){ //validamos las sesiones correctas del usuaario
          redirect(base_url()."inicio/paciente");
    }
    $this->load->model('login_model');*/
