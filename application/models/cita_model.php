<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cita_model extends CI_Model {
   public function __construct()
   {
      parent::__construct();
   }
   public function registrar($data)
   {
      //Iniciamos la transacción.
      $this->db->trans_begin();

      //Recuperamos el id del cliente registrado.
      $cliente_id = $data['medico'];
      //Insertamos los servicios que desea adquirir el cliente.
      foreach($data['paciente'] as $paciente_id){
         $this->db->insert('cita', array(
            'idMedico' => $medico_id,
            'idPaciente' => $paciente_id,
            'asunto' =>$this->input->post('asunto'),
            'tipoConsulta' => $this->input->post('tipoConsulta'),
            'fechaHora' => date('Y-m-d H:i:s'),
            'estado' =>$this->input->post('estado')
         ));
      }
      if ($this->db->trans_status() === FALSE){
         //Hubo errores en la consulta, entonces se cancela la transacción.
         $this->db->trans_rollback();
         return false;
      }else{
         //Todas las consultas se hicieron correctamente.
         $this->db->trans_commit();
         return true;
      }
   }
}
