<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class paciente_model extends CI_Model {

public function __construct(){
  $this->load->database();
}
//subir foto
/*$config['upload_path']='./assets/picture';
$config['allowed_types']='jpg|png|jpeg|git';
$config['max_size']='2048';
$config['max_width']='4480';
$config['max_height']='4480';
$config['file_name']=$_FILES['fotopost']['name'];
$this->upload->initialize($config);
if(!empty($_FILES['fotopost']['name'])){
  if($this->upload->do_upload('fotoname')){
    $foto=$this->upload->data()
    'foto'=>$foto['file_name'],

  }
}*/

public function registrar()
   {
      //Iniciamos la transacción.
      $this->db->trans_begin();
      //Intenta insertar un cliente.
      $data= array(
        'nombre' => $this->input->post('nombre'),
        'apellido' => $this->input->post('apellido'),
        'sexo' => $this->input->post('sexo'),
        'fechaNacimiento' => $this->input->post('fechaNac'),
        'correoElectronico' => $this->input->post('email'),
        'password' => $this->input->post('password')
      );
      $this->db->insert('usuario', $data);
      //Recuperamos el id del cliente registrado.
      $usuario_id = $this->db->insert_id();
      //Insertamos los servicios que desea adquirir el cliente.
      /*foreach($data['paciente'] as $paciente_id){*/
         $data= array(
            'idPaciente' => $usuario_id,
            //'usuario_id' => $usuario_id,
            'alergias' => $this->input->post('alergias')
         );
         $this->db->insert('paciente', $data);
      //}
      if ($this->db->trans_status() === FALSE){
         //Hubo errores en la consulta, entonces se cancela la transacción.
         $this->db->trans_rollback();
         return false;
      }else{
         //Todas las consultas se hicieron correctamente.
         $this->db->trans_commit();
         return true;
      }
   }
   function getAllData() {
       $query = $this->db->query('SELECT u.idUsuario,p.idPaciente, u.nombre, u.apellido, u.sexo, u.fechaNacimiento, u.correoElectronico, u.password,p.Alergias FROM usuario u INNER join paciente p on p.idPaciente=u.idUsuario');
       return $query->result();
   }
  }
