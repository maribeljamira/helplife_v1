<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class medico_model extends CI_Model {

public function __construct(){
  $this->load->database();
}
/*public function create(){
  $this->paciente_model->createData();
  return $this->db->insert("usuario", $data);
  //return $this->db->insert("paciente", $data);
  //redirect("paciente");
}
public function create(){
  $this->paciente_model->createData1();
  //return $this->db->insert("usuario", $data);
  return $this->db->insert("paciente", $data);
  //redirect("paciente");
}*/
public function registrar()
   {
      //Iniciamos la transacción.
      $this->db->trans_begin();
      //Intenta insertar un cliente.
      $data= array(
        'nombre' => $this->input->post('nombre'),
        'apellido' => $this->input->post('apellido'),
        'sexo' => $this->input->post('sexo'),
        'fechaNacimiento' => $this->input->post('fechaNac'),
        'correoElectronico' => $this->input->post('email'),
        'password' => $this->input->post('password')

         /*'nombre' => $data['nombre'],
         'apellido' => $data['apellido'],
         'sexo' => $data['sexo'],
         'fechaNacimiento' => date('Y-m-d'),
         'correoElectronico' => $data['email'],
         'password' => $data['password']*/
      );
      $this->db->insert('usuario', $data);
      //Recuperamos el id del cliente registrado.
      $usuario_id = $this->db->insert_id();
      //Insertamos los servicios que desea adquirir el cliente.
      /*foreach($data['paciente'] as $paciente_id){*/
         $data= array(
            'idMedico' => $usuario_id,
            //'usuario_id' => $usuario_id,
            'idEspecialidad' => $this->input->post('especialidad'),
            'ci' => $this->input->post('ci'),
            'nroColegiado' => $this->input->post('nroColegiado'),
            'direccion' => $this->input->post('direccion')

         );
         $this->db->insert('paciente', $data);
      //}
      if ($this->db->trans_status() === FALSE){
         //Hubo errores en la consulta, entonces se cancela la transacción.
         $this->db->trans_rollback();
         return false;
      }else{
         //Todas las consultas se hicieron correctamente.
         $this->db->trans_commit();
         return true;
      }
   }
   function getAllData() {
       $query = $this->db->query('SELECT u.idUsuario,p.idPaciente, u.nombre, u.apellido, u.sexo, u.fechaNacimiento, u.correoElectronico, u.password,m.idEspecialidad,m.ci,m.nroColegiado,m.direccion FROM usuario u INNER join medico m on m.idMedico=u.idUsuario');
       return $query->result();
   }
  }
