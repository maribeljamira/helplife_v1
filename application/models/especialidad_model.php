<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class especialidad_model extends CI_Model {
  public function __construct()
  {
    //parent::__construct();
      $this ->load ->database();
  }


 function createData(){
    $data= array(
      'nombre' =>$this->input->post('nombre')  );
      $this->db->insert('especialidad',$data);
  }
  function getAllData() {
      $query = $this->db->query('SELECT * FROM especialidad');
      return $query->result();
  }

  function getData($id) {
      $query = $this->db->query('SELECT * FROM especialidad WHERE `idEspecialidad` =' .$id);
      return $query->row();
  }

  function updateData($id) {
      $data = array (
        'nombre' =>$this->input->post('nombre')  );


      $this->db->where('idEspecialidad', $id);
      $this->db->update('especialidad', $data);
  }

  function deleteData($id) {
      $this->db->where('idEspecialidad', $id);
      $this->db->delete('especialidad');
  }
}
