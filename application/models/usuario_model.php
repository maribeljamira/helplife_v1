<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class usuario_model extends CI_Model {
    public function __construct() {
        $this->load->database();
    }
    //subir foto
    /*$config['upload_path']='./assets/picture';
    $config['allowed_types']='jpg|png|jpeg|git';
    $config['max_size']='2048';
    $config['max_width']='4480';
    $config['max_height']='4480';
    $config['file_name']=$_FILES['fotopost']['name'];
    $this->upload->initialize($config);
    if(!empty($_FILES['fotopost']['name'])){
      if($this->upload->do_upload('fotoname')){
        $foto=$this->upload->data()
        'foto'=>$foto['file_name'],

      }
    }*/
    function createData() {
        $data = array (
            'nombre' => $this->input->post('nombre'),
            'apellido' => $this->input->post('apellido'),
            'sexo' => $this->input->post('sexo'),
            'fechaNacimiento' => $this->input->post('fechaNac'),
            'correoElectronico' => $this->input->post('email'),
            'password' => $this->input->post('password'),
            'perfil' =>"defect.jpg"

          //  'rol' => $this->input->post('bio')
        );
        $this->db->insert('usuario', $data);
    }

    function getAllData() {
        $query = $this->db->query('SELECT * FROM usuario');
        return $query->result();
    }

    function getData($id) {
        $query = $this->db->query('SELECT * FROM usuario WHERE `idUsuario` =' .$id);
        return $query->row();
    }

    function updateData($id) {
        $data = array (
          'nombre' => $this->input->post('nombre'),
          'apellido' => $this->input->post('apellido'),
          'sexo' => $this->input->post('sexo'),
          'fechaNacimiento' => $this->input->post('fechaNac'),
          'correoElectronico' => $this->input->post('email'),
          'password' => $this->input->post('password')
        //  'perfil' => $this->input->post('bio')
        //  'rol' => $this->input->post('bio')
        );
        $this->db->where('idUsuario', $id);
        $this->db->update('usuario', $data);
    }

    function deleteData($id) {
        $this->db->where('idUsuario', $id);
        $this->db->delete('usuario');
    }
}
